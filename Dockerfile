ARG PHP_VERSION=7.4

# -----------------------------------------------------
# App Itself
# -----------------------------------------------------

FROM php:$PHP_VERSION-fpm-alpine

ENV REQUIRED_PACKAGES="zlib-dev curl supervisor pcre linux-headers mysql-dev rabbitmq-c oniguruma nginx"
ENV DEVELOPMENT_PACKAGES="git zip autoconf g++ make openssh-client tar python3 py3-pip pcre-dev rabbitmq-c-dev icu-dev icu-dev gettext-dev libzip-dev"
ENV PECL_PACKAGES="redis amqp apcu"
ENV EXT_PACKAGES="zip sockets pdo_mysql bcmath opcache gettext intl exif pcntl"

ENV DOCKER=true

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_NO_INTERACTION 1
ENV COMPOSER_CACHE_DIR /tmp

# Install Packages
RUN apk add --update $REQUIRED_PACKAGES $DEVELOPMENT_PACKAGES

# Fix Iconv
RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted gnu-libiconv
ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

# Update ulimit
RUN ulimit -n 16384

# Install Supervisor
RUN pip3 install supervisor-stdout

# Install Pecl Packages
RUN yes '' | pecl install -f $PECL_PACKAGES
RUN docker-php-ext-enable $PECL_PACKAGES

# Install Non-Pecl Packages
RUN docker-php-ext-install $EXT_PACKAGES

# Download composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN if [[ -z "$DECORATE_WORKERS" ]]; then \
    echo "decorate_workers_output = no" >> /usr/local/etc/php-fpm.d/docker.conf; fi

# Delete Non-Required Packages
RUN apk del $DEVELOPMENT_PACKAGES